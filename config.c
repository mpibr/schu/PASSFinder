//
//  config.c
//  PASSFinder
//
//  Created by Georgi Tushev on 07/09/16.
//  Copyright © 2016 Scientific Computing. All rights reserved.
//

#include "config.h"

const char VERSION[] = "v0.01";
const char PROGRAM_NAME[] = "PASSFinder";

void aux_parse(aux_config_t *aux, int argc, const char *argv[])
{
    /* initialize default auxiliary values */
    aux->file_in = NULL;
    aux->file_mask = NULL;
    aux->polysize = AUX_DEFAULT_POLYSIZE;
    aux->masksize = AUX_DEFAULT_MASKSIZE;
    aux->mapq = AUX_DEFAULT_MAPQ;

    if ((argc != 2) && (argc < 4))
    {
      fprintf(stderr, "Error: provide input, output and mask\n");
      help();
    }


    /* parse input pairs */
    for(int i = 1; i < argc; i++)
    {

        // current paramter length
        uint64_t parameterLength = strlen(argv[i]);

        // check each parameter and its value
        if((PARAMETER_CHECK("-h", 2, parameterLength)) ||
           (PARAMETER_CHECK("--help", 6, parameterLength)))
        {
            help();
        }
        else if((PARAMETER_CHECK("-v", 2, parameterLength)) ||
                (PARAMETER_CHECK("--version", 9, parameterLength)))
        {
            version();
        }
        else if((PARAMETER_CHECK("-q", 2, parameterLength)) || (PARAMETER_CHECK("--mapq", 6, parameterLength)))
        {
            i += 1;
            aux->mapq = (uint32_t)strtoul(argv[i], NULL, 10);
        }
        else if((PARAMETER_CHECK("-p", 2, parameterLength)) ||
                (PARAMETER_CHECK("--polysize", 10, parameterLength)))
        {
            i += 1;
            aux->polysize = (uint32_t)strtoul(argv[i], NULL, 10);
        }
        else if((PARAMETER_CHECK("-m", 2, parameterLength)) ||
                (PARAMETER_CHECK("--masksize", 10, parameterLength)))
        {
            i += 1;
            aux->masksize = (uint32_t)strtoul(argv[i], NULL, 10);
        }
        else if((PARAMETER_CHECK("-i", 2, parameterLength)) ||
                (PARAMETER_CHECK("--input", 7, parameterLength)))
        {
            i += 1;
            aux->file_in = argv[i];
        }
        else if((PARAMETER_CHECK("-r", 2, parameterLength)) ||
                (PARAMETER_CHECK("--regions", 10, parameterLength)))
        {
            i += 1;
            aux->file_mask = argv[i];
        }
        else
        {
            fprintf(stderr, "Error:: Unknown parameter %s\n", argv[i]);
            help();
        }
    }

    return;
}



void version(void)
{
    fprintf(stderr, "PASSFinder %s\n", VERSION);
    exit(EXIT_SUCCESS);
}

void help(void)
{

    fprintf(stderr, "\nTool: PASSFinder\n");
    fprintf(stderr, "Version: %s\n", VERSION);
    fprintf(stderr, "Summary:\n");
    fprintf(stderr, "\tCluster PolyA supported sites from 3'End sequencing.\n");
    fprintf(stderr, "\nUsage:\t%s [OPTIONS] -i/--input <bam/stdin> -m/--maskpoly <bed> -o/--output <bed/stdout>\n", PROGRAM_NAME);
    fprintf(stderr, "Options:\n");
    fprintf(stderr, "\t-i/--input BAM/stdin file [required]\n");
    fprintf(stderr, "\t-o/--output BED/stdout file [required]\n");
    fprintf(stderr, "\t-m/--maskpoly BED/stdin file with genomic poly regions [required]\n");
    fprintf(stderr, "\t-w/--window\tsliding window size\n");
    fprintf(stderr, "\t-d/--depth\tminimum number of polyA reads to define PASS\n");
    fprintf(stderr, "\t-p/--polysize\tminimum number of As for tail and Ts for head to define poly stretch\n");
    fprintf(stderr, "\t-h/--help\tprint help message\n");
    fprintf(stderr, "\t-v/--version\tprint current version\n");
    exit(EXIT_SUCCESS);
}

int chomp(char *line_buf)
{
    size_t  line_length;
    line_length = strlen(line_buf) - 1;
    if(line_buf[line_length] == '\n')
        line_buf[line_length] = '\0';

    return 0;
}
