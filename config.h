//
//  config.h
//  PASSFinder
//
//  Created by Georgi Tushev on 07/09/16.
//  Copyright © 2016 Scientific Computing. All rights reserved.
//

#ifndef config_h
#define config_h

#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

// define some constants
#define MAX_LENGTH 1024
#define AUX_DEFAULT_POLYSIZE 3
#define AUX_DEFAULT_MASKSIZE 3
#define AUX_DEFAULT_MAPQ 0

// define MIN & MAX macros
#define MIN(a,b) (((a)<(b))?(a):(b))
#define MAX(a,b) (((a)>(b))?(a):(b))

// define our parameter checking macro
#define PARAMETER_CHECK(param, paramLen, actualLen) (strncmp(argv[i], param, MIN(actualLen, paramLen))== 0) && (actualLen == paramLen)


// define parameters
typedef struct _aux_config_t
{
    const char    *file_in;
    const char    *file_mask;
    uint32_t masksize;
    uint32_t polysize;
    uint32_t mapq;
} aux_config_t;

// function declarations
void aux_parse(aux_config_t *, int, const char **);
void help(void);
void version(void);
int chomp(char *);

#endif /* config_h */
