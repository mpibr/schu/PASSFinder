//
//  main.c
//  PASSFinder
//
//  Created by Georgi Tushev on 07/09/16.
//  Copyright © 2016 Scientific Computing. All rights reserved.
//

#include <stdio.h>
#include <time.h>

#include "config.h"
#include "htsutils.h"

int main(int argc, const char * argv[])
{
    aux_config_t aux;
    hts_config_t hts;


    // parse auxiliary configuration
    aux_parse(&aux, argc, argv);

    clock_t begin = clock();

    // initialize with hts
    hts_init(&hts, &aux);

    // parse with hts
    hts_parse(&hts, &aux);

    // dump hash
    hts_printout(&hts);

    // free
    hts_destroy(&hts);

    clock_t end = clock();

    double time_spent = (double)(end - begin) / CLOCKS_PER_SEC;

    fprintf(stderr, "Time: %.4f sec\n", time_spent);

    return 0;
}
