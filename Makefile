# define the C compiler to use
CC=gcc

# define any compile-time flags
CFLAGS=-Wall -g -std=c99
CLIBS=-lz -lhts

# define the C source files
SRCS=main.c config.c htsutils.c

# define the C object files
OBJS=$(SRCS:.c=.o)

# define the executable file
MAIN=PASSFinder

all:	$(MAIN)
$(MAIN):	$(OBJS)
	$(CC) $(CFLAGS) -o $(MAIN) $(OBJS) $(CLIBS)

.c.o:
	$(CC) $(CFLAGS) -c $< -o $@

.PHONY: clean
clean:
	rm -f *.o $(MAIN)
