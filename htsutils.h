//
//  htsutils.h
//  PASSFinder
//
//  Created by Georgi Tushev on 07/09/16.
//  Copyright © 2016 Scientific Computing. All rights reserved.
//

#ifndef htsutils_h
#define htsutils_h

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <ctype.h>

#include <htslib/tbx.h>
#include <htslib/sam.h>
#include <htslib/bgzf.h>
#include <htslib/hts.h>
#include <htslib/regidx.h>
#include <htslib/khash.h>

#include "config.h"

#define POLY_HEAD 0
#define POLY_TAIL 1

typedef struct _pileup_t
{
    uint32_t    tid;
    uint32_t    base;
    uint32_t    reads;
    uint32_t    pass;
    uint8_t     poly;
} pileup_t, *pileup_p;

KHASH_MAP_INIT_INT64(pileup_h, pileup_p);

typedef struct _hts_config_t
{
    samFile *fh_in;
    bam_hdr_t *bam_hdr;
    regidx_t *tabix;
    bam1_t *bam;
    khash_t(pileup_h) *pstrand;
    khash_t(pileup_h) *nstrand;
} hts_config_t;

void hts_init(hts_config_t *, aux_config_t *);
void hts_parse(hts_config_t *, aux_config_t *);
void hts_dumphash(khash_t(pileup_h) *, bam_hdr_t *, const char);
void hts_freehash(khash_t(pileup_h) **);
void hts_printout(hts_config_t *);
void hts_destroy(hts_config_t *);

//int parseBAM(Settings *);
void dumpBamSeq(bam1_t *);

static inline uint32_t findPoly(bam1_t *, int, int);
static inline uint32_t bam_calqlen(const bam1_core_t *, const uint32_t *);
static inline uint32_t bam_calend(const bam1_core_t *, const uint32_t *);

#endif /* htsutils_h */
