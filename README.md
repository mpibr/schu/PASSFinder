# PASSFinder

## Dependencies

### [htslib](https://github.com/samtools/htslib)
Install HTSLib, so that include files are available under /usr/local/include and library file is available under /usr/local/lib. Make sure that those paths are in your search path. 

Linux: from source and follow instructions

Mac: ```brew install htslib```

### [zlib](https://zlib.net/)
Install Zlib

Linux: ```apg-get install zlib1g-dev```

Mac: 

either install developer tools with
```xcode-select --install```

or use Homebrew ```brew install zlib```
 
## Compilation

Makefile is provided. ```make``` command will create an executable.

## Help
Tool: **PASSFinder**

Version: **v0.01**

Summary: **Cluster PolyA supported sites from 3'End sequencing.**

Usage:	

```PASSFinder [OPTIONS] -i/--input <bam/stdin> -m/--maskpoly <bed> -o/--output <bed/stdout>```

Options:

	-i/--input BAM/stdin file [required]
	-o/--output BED/stdout file [required]
	-m/--maskpoly BED/stdin file with genomic poly regions [required]
	-w/--window	sliding window size
	-d/--depth	minimum number of polyA reads to define PASS
	-p/--polysize	minimum number of As for tail and Ts for head to define poly stretch
	-h/--help	print help message
	-v/--version	print current version
	
## Bug reports
Scientific Computing Facility @ Max-Planck Institute For Brain Research

<sciclist@brain.mpg.de>
